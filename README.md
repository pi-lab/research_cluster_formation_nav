# 集群无人机编队控制与协同导航

### 1. 研究目标

集群无人机在民用、军事等领域起到越来越重要的作用，能够突破单机飞行时间短、可靠性低、传感能力弱等限制，多个无人机间能力进行集中，并协同完成更为复杂的任务。

本研究课题针对集群无人机的编队控制、协同导航开展研究工作，主要的研究工作包括：

1. 集群保持、同时到达、编队精确控制；
2. 使用视觉、UWB、IMU等实现编队的高精度、高可靠导航定位；
3. 集中式或者分布式相互定位。

![cover images](images/cluster_nav.png)



### 2. 主要思路

研究思路：

1. 先通读一下主要的参考文献，建立对所研究问题的基本认识，了解基本的方法等。
2. 找一些代码运行一下，建立直觉的认识，并熟悉数据集。
3. 可以从基本的编队控制、集群SLAM开始，然后再深入到分布式SLAM。

具体的研究方法（需要尝试）：

1. 学习无人机的仿真、控制。可以使用Gazebo，PX4，在仿真环境里控制无人机飞行
2. 研究单机的飞行控制，通过发送控制指令控制无人机按照特定的轨迹、速度等飞行
3. 研究无人机的编队集结，通过做[编队无人机的集结](https://gitee.com/pi-lab/learn_programming/tree/master/4_projects#23-%E7%BC%96%E9%98%9F%E6%97%A0%E4%BA%BA%E6%9C%BA%E7%9A%84%E9%9B%86%E7%BB%93)项目熟悉编队集结的算法，仿真，控制程序的整合。
4. 学习视觉SLAM，运行VINS，ORB-SLAM等，掌握SLAM的理论、方法
5. 学习协同SLAM方法



## 3. 关键技术

1. 集群保持、同时到达、编队精确控制

2. 视觉SLAM

3. 协同SLAM




## 研究内容

* [集群编队控制](formation_control)
* [基于LLM的无人机集群控制](LLM_Control)

## 参考资料

### 论文

* 协同SLAM
    * 2023 D^2SLAM: Decentralized and Distributed Collaborative Visual-inertial SLAM System for Aerial Swarm
    * 2023 - COVINS-G: A Generic Back-end for Collaborative Visual-Inertial SLAM
    * 2022 - Distributed Riemannian Optimization with Lazy Communication for Collaborative Geometric Estimation



### 学习资料、课程

* [《一步一步学SLAM》](https://gitee.com/pi-lab/learn_slam)
* [《一步一步学ROS》](https://gitee.com/pi-lab/learn_ros)
* [练习项目-编队无人机的集结](https://gitee.com/pi-lab/learn_programming/tree/master/4_projects#23-%E7%BC%96%E9%98%9F%E6%97%A0%E4%BA%BA%E6%9C%BA%E7%9A%84%E9%9B%86%E7%BB%93)
* [《一步一步学飞控》](https://gitee.com/pi-lab/research_flight_controller)
* 飞行器 - 环境 - 一体化仿真系统 - https://gitee.com/pi-lab/research_uav_cv_simulation
* 无人机编队控制与仿真系统 https://gitee.com/pi-lab/uav-css
* FastGCS https://gitee.com/pi-lab/FastGCS
* 无人机自主飞行 - https://gitee.com/pi-lab/uav-autonomous-flight
* 集群、仿真等资料、代码 http://192.168.1.3/PI_LAB/project---formation-flight/blob/master/programs_soft.md
* [《无人机自主导航》](https://gitee.com/pi-lab/research_autonomous_navigation)：研究视觉SLAM和无人机控制，实现无人机在卫星导航失效的情况下的自主定位与控制



### 程序

* https://tinympc.org/
    * https://github.com/TinyMPC/TinyMPC
* https://gitee.com/aircraft-is-design/CppRobotics
* https://gitee.com/aircraft-is-design/PythonRobotics
* [SwarmLab](https://github.com/lis-epfl/swarmlab) 
