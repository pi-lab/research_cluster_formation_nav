# 基于LLM的控制方法

目前的控制方法都是人工编写的代码，这样灵活性比较低，无法应对复杂的集群系统。考虑如何使用LLM，帮助快速生成感知、决策、控制代码。最终的样子就是人通过自然语言和系统交互，自动生成框图（MBSE，或者Simulink里面的框图），然后再生成代码。


## 研究思路

随着人工智能中LLM，CoT等技术的发展，通过LLM+Workflow+Agent等技术实现代码的自动设计、生成、测试、部署、运维等越来越可能实现。
* 和端到端的程序相比，这样的方法生成的代码对特定问题的泛化能力更强，鲁棒性可能更好
* 可解释性更好，由多个模块组成，可以单独维护小模块
* 端到端的系统可以通过一个可解释模块，但是从底层原理上不太一样。代码生成的方式是从最底层安装逻辑、操作等实现处理流程；而端到端的可解释性从额外的模块

具体的流程：
* 让LLM通过知识库学习，MBSE、UML等技术，学习软件系统设计的方法等
* 提出系统的要求，通过CoT生成子系统、模块、关键技术
* 生成模块的定义，然后分模块继续细化
* 将颗粒度比较细的模块、算法通过编程实现，分装成模块，然后自动生成代码进行单元测试、集成测试。。。
* 系统集成、虚拟数据测试


## RAG+Robot

RAG: Retrieval Augmented Generation 检索增强生成

通过检索相关的知识并将其融入Prompt(向模型提供输入以引导其生成特定输入的文本或指令)，让大模型能够参考相应的知识从而给出合理回答。RAG的核心为"检索+生成"，前者主要是利用向量数据库的高效存储和检索能力，召回目标知识；后者则是利用大模型和Prompt工程，将召回的知识合理利用，生成目标答案。

![image1](./images/image1.png)

### 2024年代码开源：Agent和RAG结合的机器人控制系统

机械手臂ROS后端，仿真模拟。

使用语音的方式，控制机械手臂进行一系列操作。

缺点：

有的语音回复，会前于动作进行回复；

有的语音命令，他能够回答，但是在动作上并没有实现。

### 2024 Embodied Agents：利用大模型实现开放物理世界机器人操作

这篇文章的思路很像 topic 2 论文。

![image2](./images/image2.png)

### 2024 具身智能SayPlan: ChatGPT + Robotics + 3D Scene Graphs

这个视频，通过文字输入，对机器人进行指令的控制，机器人通过语言搜索、迭代重规划、任务规划的方法，完成指令。



## 集群无人机应用，控制

### 无人机集群：如何突破无人机规模的极限？无限爆兵和应用？

实现无人机的集群智能化，减少人手不足的问题。

### 无人机集群智能控制

在视频中，看到了无人机集群的控制，是根据系统进行指令的下达，在这能够区结合输入语音或者文字的描述，对无人机集群进行控制。

![image3](./images/image3.png)

### 集群无人机飞行控制

在视频中，进行的是定点控制无人机的飞行，飞行的轨迹和状态应该是固定的。

![image4](./images/image4.png)

### 【集群协同】区域覆盖搜索围捕“练习生”

这里，可以结合topic3的论文，先进行图像的预处理，然后进行LLM结合进行无人机集群协同。


## 参考资料

### RAG + Robot

- 2024 代码开源：我做了一个Agent和RAG结合的机器人控制系统  https://b23.tv/LQd1Gl6

- 2024 Embodied Agents：利用大模型实现开放物理世界机器人操作  https://b23.tv/FDzV2O2

- 2024 具身智能SayPlan: ChatGPT + Robotics + 3D Scene Graphs https://b23.tv/VYVWSHq


### LLM + Code Generation

* Large language model-based code generation for the control of construction assembly robots: A hierarchical generation approach
	- https://www.sciencedirect.com/science/article/pii/S2666165924001698
	- [paper and summary](articles_related_to_large_language_models/large_language_model-based_code_generation)

* LLM-based and Retrieval-Augmented Control Code Generation
	- https://dl.acm.org/doi/10.1145/3643795.3648384

* LLM-based Control Code Generation using Image Recognition
	- https://arxiv.org/abs/2311.10401

* LLM-based and retrieval-augmented Control Code Generation
	- https://github.com/hkoziolek/LLM-CodeGen-RAG



### 集群无人机应用，控制

* 无人机集群：如何突破无人机规模的极限？无限爆兵和应用？https://www.bilibili.com/video/BV1HKtgeoE52
* 无人机集群智能控制 https://www.bilibili.com/video/BV1TkWYeoEo4
* 集群无人机飞行控制 https://www.bilibili.com/video/BV1rh1iYtEhU/
* 【集群协同】区域覆盖搜索围捕“练习生” https://www.bilibili.com/video/BV1RzsseSEqC
* 固定翼多旋翼蜂群编队集群 多旋翼无人机蜂群编队集群 https://www.bilibili.com/video/BV1NgsreyE4U
* P600无人机集群挑战密林避障！极限环境下的智能飞行算法实战演示 https://www.bilibili.com/video/BV1ZLHDenED7
* 具备避让机器人功能的多机器人集群路径规划 https://www.bilibili.com/video/BV1mr421b7az
