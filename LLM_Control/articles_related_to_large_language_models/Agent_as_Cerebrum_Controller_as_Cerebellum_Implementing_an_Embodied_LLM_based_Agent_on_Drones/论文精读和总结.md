# Articles 3

《**大脑作为智能体(代理)，小脑作为控制器：在无人机上实现一种基于嵌入式lmm的智能体**》

## 1.现有方法存在的问题：

1.在端到端机器人学习中，任务通常是通过模仿学习或强化学习策略获得的，需要积累特定于任务的数据集，在这些领域，创建和利用特定任务的数据集是解决不同挑战的标准。然而，这种范式遇到了一些限制：

(1)大规模带注释的数据是端到端训练的基本要求，在许多场景下可能不可行或不实用。

(2)将知识和技能推广到新任务或不同机器人平台的能力仍然非常低效。

(3)由于模拟环境与现实环境之间的差异，模拟到现实的过渡带来了相当大的挑战。

2.在为特定任务（如无人机）设计量身定制的机器人方面仍然存在显著的挑战。

(1)这些大容量模型往往表现出较慢的响应能力，这可能无法满足某些机器人应用对精确和快速控制的要求，比如，需要持续稳定的无人机必须在短至0.01秒的间隔内实现控制。

(2)利用这些模型的系统通常缺乏冗余，无法有效应对运行过程中可能出现的不可预见的情况。



## 2.先验知识

1. 大语言模型（LLM）

大语言模型可以接受输入、进行分析或推理，最终进行输出。比如：ChatGPT和Kimi，但是大语言模型无法像人类一样，拥有规划思考能力、运用各种工具与物理世界互动，以及拥有人类的记忆能力。

LLM：接受输入、思考、输出

人类：LLM（接受输入、思考、输出）+记忆+规划+工具

2. AI智能体（Agent）

Agent可以翻译为代理人，是可以帮助我们完成一些事情的实体，可以是人或机器。可以将它理解为一个智能助手，只需要我们给出任务，它就可以自己做出决策并执行。

没有智能体前，我们要不就是人工处理，每一步都人工去做，要么写一段提示词，让大模型进行信息整理，写大纲，写章节，起标题，并进行修改。

但是有以下缺点：

我们需要用不同的提示词来完成不同的任务。
大模型没有记忆能力，有上下文限制。
提示词会非常复杂，不利于维护。所以我们引出智能体的概念。

3. 智能体的核心公式：

Agent（智能体）= LLM（大模型）+ Memory（记忆）+  Planning（规划）+ Tools（工具）

**记忆（Memory）**
短期记忆：执行任务过程中的上下文，会在子任务的执行过程产生和暂存，在任务完结后被清空。
长期记忆：长时间保留的信息，一般是指外部知识库。
**规划（Planning）**
智能体会把大型任务分解为子任务，并规划执行任务的流程；**智能体能体会对任务执行的过程进行思考和反思**，从而决定是继续执行任务，或判断任务完结并终止运行。
简单来说，就是我们上面提到的写作流程，收集、写大纲，写章节等等，俗称工作流。
**工具（Tools）**
为智能体配备工具 API，比如：计算器、搜索工具、代码执行器、数据库查询工具等。有了这些工具 API，智能体就可以和物理世界交互，解决实际的问题。

## 3.文章的思路：

1. 整体架构

![image15](../images/image15.png)

2. Agent

   使用 LM-Based Agent（LLM和LMM）作为中央大脑或决策系统，其能够展示推理和计划能力，利用思维链（CoT）和问题分解等方法，记忆被看作是获取、保存、检索和利用信息的机制，使得智能体具备短时学习的能力，能够快速适应新任务。

3. Controller

   控制器，根据传感器提供的姿态信息，评估当前状态，并命令执行器相应地修改飞机的方向。

4. Aeroagent

   

## 4.总结



## 5.关于论文和思路，一些疑问点

