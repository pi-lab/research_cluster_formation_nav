# Topic 4

《**Large language model-based code generation for the control of construction assembly robots: A hierarchical generation approach**》

## 1.研究背景

大规模无人机群管理方法需要更先进的方法。

## 2.主要方法

论文提出：

通过 LLMS 处理自然语言指令，生成期望的几何图案；

使用有符号距离函数( SDF )将语言指令转换为几何图案，使用 SDF 来引导无人机排列成指定的形状；

使用集群算法确保无人机不仅能达到目标集合形状，还能均匀分布，避免碰撞。

## 3.实验设计

设计了一个控制系统：

LLM接口：用户通过自然语言于系统交互，模型处理这些指令并生成对应的几何图案，使用python实现SDF。

集群算法：使用集群算法，引导无人机群集体运动，使用SDF的矢量生成，提供目标表面的方向和距离，确保无人机之间的平稳并避免碰撞。

unity：模拟64架无人机进行实验。

真实环境：使用crazyfile无人机群进行测试，证明可行性。

## 4.局限性

对LLM的依赖性很高，LLM的生成结果，会直接影响无人机行为的准确性；

在现实环境中，还有很大的提升空间。