# 智能蜂群控制



研究蜂群的控制方法，能够饱和攻击特定的目标，编队避开危险，执行多种任务。



![consensus_formation.png](images/consensus_formation.png)



![flock.gif](images/flock.gif)


## 研究目标，研究意义

在未来的现代战争中，无人机起到了很大的作用。 我们通过研究智能无人机集群的控制，在军用方面第一波无人机集群能吸引敌方防空火力，之后的无人机集群能够饱和攻击指定的目标，实现低成本、少伤亡的精准打击。基于领航跟随法与人工势场法的研究，无人机集群可以协同避开障碍，寻找最优路径，高速、快捷的执行各种任务。在未来的地形探测、情报收集、快速打击等方面有着重要的作用。


## 研究内容

1. 通过对 Boids 模型的研究，通过无人机集群的协同飞行，利用Matlab和C++等建立定向集群飞行模型。在此基础上增加无人机数量、增大或减小无人机之间的间距、实现特定图案的表演飞行、实现向特定目标点的飞行任务
2. 通过对于领航跟随法和人工势场法研究，实现无人机集群在空中飞行的协同避障。可以参考APF路径规划与动态规划算法，实现路径规划

## 参考代码

* [蜂群飞行](code/Flocking)
* [编队飞行与避障](code/consensus_formation)
* [练习项目-编队无人机的集结](https://gitee.com/pi-lab/learn_programming/tree/master/4_projects#23-%E7%BC%96%E9%98%9F%E6%97%A0%E4%BA%BA%E6%9C%BA%E7%9A%84%E9%9B%86%E7%BB%93)
* [SwarmLab](https://github.com/lis-epfl/swarmlab) 

## 参考资料

* 基于领航跟随法与人工势场法的的多机器人编队协同避障 https://www.bilibili.com/video/BV16m411z7e7

* Boids Background and Update -
  - http://www.red3d.com/cwr/boids/
* Boids: An Implementation of Craig W. Reynolds' Flocking Model
  - http://pages.cs.wisc.edu/~psilord/lisp-public/boids.html
* Flocks, Herds, and Schools: A Distributed Behavioral Model
  - http://www.cs.toronto.edu/~dt/siggraph97-course/cwr87/



### 关键词

* 领航跟随法

* 人工势场法

* 多机器人编队协同避障

* 多无人机编队协同避障



### 编程

* 一步一步学编程 https://gitee.com/pi-lab/learn_programming
* Qt https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/qt
* 编程小项目 https://gitee.com/pi-lab/learn_programming/tree/master/4_projects
* Git操作教程 https://gitee.com/pi-lab/learn_programming/tree/master/6_tools/git



### 相关研究视频

* 无人机集群：如何突破无人机规模的极限？无限爆兵和应用？ https://www.bilibili.com/video/BV1HKtgeoE52