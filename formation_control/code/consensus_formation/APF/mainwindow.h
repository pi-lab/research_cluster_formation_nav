#pragma once

#include <QMainWindow>
#include <QPlainTextEdit>
#include <QMutex>

#include "apf.h"

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);

protected:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void timerEvent(QTimerEvent *event);


private:
    int             canvasWidth, canvasHeight;
    QImage          *m_img;

    std::vector<NS_APF::Vec2> position_box;
};

