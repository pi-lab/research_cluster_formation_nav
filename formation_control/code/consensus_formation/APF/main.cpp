#include "mainwindow.h"
#include "apf.h"

#include <QApplication>
#include <unistd.h>
#include <QtGui>
#include <QMainWindow>
#include <QDebug>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // create main window
    MainWindow  win;
    win.setWindowTitle("APF");
    win.show();

    // begin Qt GUI loop
    app.exec();

    return 0;
}


