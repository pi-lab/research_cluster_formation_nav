# Consensus Formation - Leader Follower - APF

基于领航跟随法与人工势场法的的多机器人编队协同避障 https://www.bilibili.com/video/BV16m411z7e7

![img](images/consensus_formation.png)


## [Matlab 代码](matlab)

For Ubuntu system, please install octave
```
sudo apt-get install -y octave
```

In Octave, run:
```
consensus_APF
```
