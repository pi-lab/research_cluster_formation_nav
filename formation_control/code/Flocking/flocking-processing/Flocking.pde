/**
 * Flocking 
 * by Daniel Shiffman.  
 * 
 * An implementation of Craig Reynold's Boids program to simulate
 * the flocking behavior of birds. Each boid steers itself based on 
 * rules of avoidance, alignment, and coherence.
 * 
 * Click the mouse to add a new boid.
 *
 * References:
 *   Boids Background and Update - 
 *     http://www.red3d.com/cwr/boids/
 *   Boids: An Implementation of Craig W. Reynolds' Flocking Model
 *     http://pages.cs.wisc.edu/~psilord/lisp-public/boids.html
 *   Flocks, Herds, and Schools: A Distributed Behavioral Model
 *     http://www.cs.toronto.edu/~dt/siggraph97-course/cwr87/
 */

Flock flock;

void setup() {
  size(640, 360);
  flock = new Flock();
  // Add an initial set of boids into the system
  for (int i = 0; i < 150; i++) {
    flock.addBoid(new Boid(width/2,height/2));
  }
}

void draw() {
  background(50);
  flock.run();
}

// Add a new boid into the System
void mousePressed() {
  flock.addBoid(new Boid(mouseX,mouseY));
}
