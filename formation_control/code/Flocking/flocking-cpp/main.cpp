#include <QApplication>
#include <QtGui>

#include "mainwindow.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    // create main window
    MainWindow  win;
    win.setWindowTitle("Flocking");
    win.show();

    // begin Qt GUI loop
    app.exec();

    return 0;
}
