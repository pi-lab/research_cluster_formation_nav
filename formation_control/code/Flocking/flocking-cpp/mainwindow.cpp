
#include <stdio.h>
#include <stdlib.h>

#include <QThread>
#include <QtGui>
#include <QDebug>

#include "mainwindow.h"


////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    // create background image as canvas
    canvasWidth  = 800;
    canvasHeight = 600;
    m_img = new QImage(canvasWidth, canvasHeight,
                       QImage::Format_RGB888);
    m_img->fill(QColor(0xff, 0xff, 0xff));
    setGeometry(20, 35, canvasWidth+20, canvasHeight+20);

    // set window properties
    setBackgroundRole(QPalette::Base);
    setAutoFillBackground(true);

    // set focus to self
    setFocus();

    // start timer (lower value for fast simulation speed)
    startTimer(30);

    // create boids & set default width & height
    for(int i=0; i<150; i++) {
        Boid b(canvasWidth/2.0, canvasHeight/2.0);
        b.width = canvasWidth;
        b.height = canvasHeight;

        flock.addBoid(b);
    }

}


void MainWindow::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event);
    static int step_i = 0;

    // run flock new time step calculation
    flock.run();

    // draw flock
    QPainter    painter;
    m_img->fill(QColor(0xff, 0xff, 0xff));

    painter.begin(m_img);
    painter.setRenderHint(QPainter::Antialiasing, true);

    // draw each boid
    for(auto &b : flock.getBoids()) {
        painter.setPen(QColor(255, 0, 0));
        painter.drawRect(b.position.x-2, b.position.y-2, 4, 4);
    }

    painter.end();

    // save images
    if( 0 ) {
        char img_name[256];
        sprintf(img_name, "img_%06d.jpg", step_i++);
        m_img->save(QString::fromLocal8Bit(img_name));
    }

    // update to mainwindow
    this->update();
}


void MainWindow::keyPressEvent(QKeyEvent *event)
{
    QPainter    painter;

    // 'o' - random draw rectangles
    if( event->key() == Qt::Key_O ) {
        painter.begin(m_img);
        painter.setRenderHint(QPainter::Antialiasing, true);

        double      x1, y1, x2, y2, w, h;

        for(int i=0; i<1000; i++) {
            painter.setPen(QColor(rand()%255, rand()%255, rand()%255, rand()%255));

            x1 = rand() % 800; y1 = rand() % 600;
            x2 = rand() % 800; y2 = rand() % 600;
            w = x2 - x1;
            h = y2 - y1;
            painter.drawRect(x1, y1, w, h);
        }

        painter.end();
    }

    // 'c' - clean canvas
    if( event->key() == Qt::Key_C ) {
        m_img->fill(QColor(0xff, 0xff, 0xff));
    }

    // 'q' - quit the program
    if( event->key() == Qt::Key_Q ) {
        this->close();
    }

    this->update();
}

void MainWindow::mousePressEvent(QMouseEvent *event)
{
    Boid b(event->x(), event->y());
    b.width = canvasWidth;
    b.height = canvasHeight;
    flock.addBoid(b);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    Q_UNUSED(event);

    QPainter    painter(this);

    // draw offline image to screen
    painter.drawImage(QPoint(0, 0), *m_img);
}
