#pragma once

#include <vector>

#include "Boid.h"

class Flock {
public:
    Flock() {

    }

    void run(void) {
        for(auto &b : boids) {
            b.run(boids);
        }
    }

    void addBoid(Boid b) {
        boids.push_back(b);
    }

    std::vector<Boid>& getBoids(void) {
        return boids;
    }

public:
    std::vector<Boid> boids;
};

