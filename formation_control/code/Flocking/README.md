# Flocks, Herds, and Schools: A Distributed Behavioral Model

An implementation of Craig Reynold's Boids program to simulate
the flocking behavior of birds. Each boid steers itself based on
rules of avoidance, alignment, and coherence.

![flock](flock.gif)

Click the mouse to add a new boid.

References:
* Boids Background and Update -
    - http://www.red3d.com/cwr/boids/
* Boids: An Implementation of Craig W. Reynolds' Flocking Model
    - http://pages.cs.wisc.edu/~psilord/lisp-public/boids.html
* Flocks, Herds, and Schools: A Distributed Behavioral Model
    - http://www.cs.toronto.edu/~dt/siggraph97-course/cwr87/

Other References:
* 基于图的聚类之-Affinity Propagation（AP）聚类 https://zhuanlan.zhihu.com/p/51665667
